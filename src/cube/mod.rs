//! Contains types and utlities for working with regular NxN Rubik's-style cube-shaped
//! twisty-puzzles.

use bitvec::prelude::*;

/// A type for a regular NxN Rubik's-style cube-shaped twisty-puzzle.
///
#[derive(Clone, Debug)]
pub struct Cube {
    n: usize,
    pieces: usize,
    #[allow(dead_code)]
    data: BitVec,
}

impl Cube {
    /// Construct a new NxN cube.
    ///
    /// Example:
    /// ```
    /// use twisty_puzzle::cube::Cube;
    ///
    /// let c = Cube::new(2);
    /// assert_eq!(c.n(), 2);
    /// assert_eq!(c.pieces(), 8);
    ///
    /// let c = Cube::new(3);
    /// assert_eq!(c.n(), 3);
    /// assert_eq!(c.pieces(), 26);
    ///
    /// let c = Cube::new(7);
    /// assert_eq!(c.n(), 7);
    /// assert_eq!(c.pieces(), 218);
    /// ```
    ///
    pub fn new(n: usize) -> Self {
        // The formula for the number of pieces in a NxN Rubik's-style
        // cube-shaped twisty-puzzle is:  6x^2 - 12x + 8
        let pieces = 6 * n.pow(2) - 12 * n + 8;

        // We need log2(pieces) + 2 bits to store the piece identifer and its orientation. The
        // maximum number of orientations a piece may have is 4, so we use two bits per piece. We
        // could use less bits when possible but I don't think the complexity trade-off is worth it
        // at the moment.
        let bit_size = (pieces.ilog2() as usize + 2) * pieces;

        // Use a dynamically sized bitfield to take advantage of a whole host of optimizations,
        // ownership considerations, et al.
        let data = BitVec::with_capacity(bit_size as usize);

        Self { n, pieces, data }
    }

    /// Returns the "size" of the puzzle. A 3x3x3 puzzle returns 3.
    ///
    pub fn n(&self) -> usize {
        self.n
    }

    /// Returns how many pieces the puzzle contains.
    ///
    pub fn pieces(&self) -> usize {
        self.pieces
    }

}

#[cfg(test)]
mod test {
    use super::Cube;

    #[test]
    fn test_new() {
        let c = Cube::new(2);
        assert_eq!(c.data.capacity(), 64);

        let c = Cube::new(3);
        assert_eq!(c.data.capacity(), 192);

        let c = Cube::new(7);
        assert_eq!(c.data.capacity(), 1984);
    }
}

