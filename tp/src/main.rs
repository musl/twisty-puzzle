use anyhow::Result;
use clap::Parser;

/// A program to visualize and solve twisty puzzles
///
#[derive(Debug, Parser)]
#[command(author, version, about, long_about = None)]
// Disable the built-in automatic version flag so we can replace it.
#[command(disable_version_flag = true)]
struct Cli {
    /// Display detailed build information
    #[clap(long, short = 'V')]
    pub version: bool,
}

/// Print detailed build information stamped into this binary at build time.
///
/// Note:
/// While we're reading environment variables here, the vergen package
/// ensures that the values are not overwritten by values from the
/// environment where this binary is run.
///
fn print_version_info() {
    println!("\
        Version: {}\n\
        Time Stamp: {}\n\
        SHA: {}\n\
        Branch: {}\n\
        Profile: {}\n\
        Rust Version: {}\n\
        Features: {}\n\
        User: {}\
        ",
        env!("VERGEN_BUILD_SEMVER"),
        env!("VERGEN_BUILD_TIMESTAMP"),
        env!("VERGEN_GIT_SHA"),
        env!("VERGEN_GIT_BRANCH"),
        env!("VERGEN_CARGO_PROFILE"),
        env!("VERGEN_RUSTC_SEMVER"),
        env!("VERGEN_CARGO_FEATURES"),
        env!("VERGEN_SYSINFO_USER"),
        );
}

fn main() -> Result<()> {
    let cli = Cli::parse();

    if cli.version {
        print_version_info();
        return Ok(());
    }

    Ok(())
}
